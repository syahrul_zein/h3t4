<?php
    require_once("animal.php");
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new animal("shaun");
    echo $sheep -> name."<br>";
    echo $sheep -> legs."<br>";
    echo $sheep -> cold_blooded."<br><br>";

    $frog = new frog("buduk");
    echo $frog -> name."<br>";
    echo $frog -> legs."<br>";
    echo $frog -> cold_blooded."<br>";
    echo $frog -> jump()."<br><br>";

    $ape = new ape('kera sakti');
    echo $ape -> name."<br>";
    echo $ape -> legs."<br>";
    echo $ape -> cold_blooded."<br>";
    echo $ape -> yell();
?>